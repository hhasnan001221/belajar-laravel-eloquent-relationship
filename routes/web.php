<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CastController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\TableController;
use App\Http\Controllers\ProfilController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::controller(UserController::class)->group(function () {
    Route::get('register', 'register')->name('register');
	Route::post('register', 'registerSimpan')->name('register.simpan');
    
	Route::get('login', 'login')->name('login');
	Route::post('login', 'loginAksi')->name('login.aksi');
    
	Route::get('logout', 'logout')->middleware('auth')->name('logout');
});


Route::middleware('auth')->group(function () {
    
Route::get('/', function () {
    return view('master');
});
Route::controller(CastController::class)->prefix('casts')->group(function () {
    Route::get('', 'index')->name('casts');
    Route::get('tambah', 'tambah')->name('casts.tambah');
    Route::post('tambah', 'simpan')->name('casts.tambah.simpan');
    Route::get('edit/{id_casts}', 'edit')->name('casts.edit');
    Route::post('edit/{id_casts}', 'update')->name('casts.tambah.update');
    Route::get('hapus/{id_casts}', 'hapus')->name('casts.hapus');
});


Route::controller(ProfilController::class)->prefix('profil')->group(function () {
    Route::get('', 'index')->name('profil');
    Route::get('tambah', 'tambah')->name('profil.tambah');
    Route::post('tambah', 'simpan')->name('profil.tambah.simpan');
    Route::get('edit/{id_profil}', 'edit')->name('profil.edit');
    Route::post('edit/{id_profil}', 'update')->name('profil.tambah.update');
    Route::get('hapus/{id_profil}', 'hapus')->name('profil.hapus');
});
Route::controller(FilmController::class)->prefix('film')->group(function () {
    Route::get('', 'index')->name('film');
    Route::get('tambah', 'tambah')->name('film.tambah');
    Route::post('tambah', 'simpan')->name('film.tambah.simpan');
    Route::get('edit/{id_film}', 'edit')->name('film.edit');
    Route::post('edit/{id_film}', 'update')->name('film.tambah.update');
    Route::get('hapus/{id_film}', 'hapus')->name('film.hapus');
});
Route::controller(GenreController::class)->prefix('genre')->group(function () {
    Route::get('', 'index')->name('genre');
    Route::get('tambah', 'tambah')->name('genre.tambah');
    Route::post('tambah', 'simpan')->name('genre.tambah.simpan');
    Route::get('edit/{id_genre}', 'edit')->name('genre.edit');
    Route::post('edit/{id_genre}', 'update')->name('genre.tambah.update');
    Route::get('hapus/{id_genre}', 'hapus')->name('genre.hapus');
});

Route::get('/data-tables', [TableController::class,'index']);
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
