@extends('layout.app')

@section('title', 'Profil Page')


@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"/>   

@section('contents')
<body>
    <div class="wrapper">
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Tambah-Data-Profil</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Data-Profil</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <form action="{{ isset($profil) ? route('profil.tambah.update', $profil->id_profil) : route('profil.tambah.simpan') }}" method="post">
                    @csrf
                    <div class="row">
                      <div class="col-12">
                        <div class="card shadow mb-4">
                          <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-info"></h6>
                          </div>
                          <div class="card-body">
                            <div class="form-group">
                              <label for="nama">Umur</label>
                              <input type="text" class="form-control" id="umur" name="umur" value="">
                            </div>
                            <div class="form-group">
                              <label for="nama">Biodata</label>
                              <input type="text" class="form-control" id="bio" name="bio" value="">
                            </div>
                            <div class="form-group">
                              <label for="nama">Nama</label>
                              <input type="text" class="form-control" id="nama" name="nama" value="">
                            </div>
                          <div class="form-group">
                            <label for="id_user">ID User</label>
                                <select name="id_user" id="id_user" class="custom-select">
                                    <option value="" selected disabled hidden>-- Pilih Email User --</option>
                                        @foreach($users as $row)
                                          <option value="{{ $row->id }}">{{ $row->email }}</option>
                                        @endforeach
                               </select>
                          </div>
                          </div>
                          <div class="card-footer">
                            <button type="submit" class="btn btn-info">Simpan</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
            </section>
        </div>

        <footer class="main-footer">
            <div class="float-right d-none d-sm-block">
                <b>Version</b> 3.2.0
            </div>
            <strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights
            reserved.
        </footer>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
</body>
@endsection