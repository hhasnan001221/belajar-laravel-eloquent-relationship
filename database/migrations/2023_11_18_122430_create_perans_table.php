<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perans', function (Blueprint $table) {
            $table->id();
            $table->string('nama', 45);
            $table->unsignedBigInteger('id_film');
            $table->unsignedBigInteger('id_casts');
            $table->timestamps();

            $table->foreign('id_film')->references('id_film')->on('films');
            $table->foreign('id_casts')->references('id_casts')->on('casts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perans');
    }
};
