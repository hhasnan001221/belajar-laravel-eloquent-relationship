<?php

namespace App\Http\Controllers;

use App\Models\Profil;
use App\Models\User;
use Illuminate\Http\Request;

class ProfilController extends Controller
{
    public function index()
	{
		$profil = Profil::all();

		return view('profil.index', compact('profil'));
	}

	public function tambah()
	{
		$users = User::all();


		return view('profil.form', compact('users'));
	}

	public function simpan(Request $request)
	{
		$data = [
			'umur' => $request->umur,
			'bio' => $request->bio,
			'nama' => $request->nama,
			'id_user' => $request->id_user,
		];

		Profil::create($data);

		return redirect()->route('profil');
	}

	public function edit($id)
	{
		$profil = Profil::find($id);
		$users = User::all();


		return view('profil.form', compact('users', 'profil'));
	}

	public function update($id, Request $request)
	{
		$data = [
			'umur' => $request->umur,
			'bio' => $request->bio,
			'nama' => $request->nama,
			'id_user' => $request->id_user,
		];

		Profil::find($id)->update($data);

		return redirect()->route('profil');
	}

	public function hapus($id)
	{
		Profil::find($id)->delete();

		return redirect()->route('profil');
	}

}
