<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Cast;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    public function register()
	{
		return view('auth/register');
	}

	public function registerSimpan(Request $request)
	{
        $data = $request->all();

        $validate = Validator::make($data, [
            'nama' => 'required',
			'email' => 'required|email',
			'password' => 'required|confirmed'
        ]);

        User::create([
            'nama' => $data['nama'],
            'email' => $data['email'],
            'password' => $data['password'],
        ]);

		return redirect()->route('login');
	}

	public function login()
	{
		return view('auth/login');
	}

	public function loginAksi(Request $request)
	{
		$casts = Cast::all();

        Validator::make($request->all(), [
			'email' => 'required|email',
			'password' => 'required'
		])->validate();


		return view('casts.index', compact('casts'));
	}

	public function logout(Request $request)
	{
		Auth::guard('web')->logout();

		$request->session()->invalidate();

		return redirect('/');
	}

}
