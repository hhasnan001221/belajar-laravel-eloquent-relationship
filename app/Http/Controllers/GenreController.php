<?php

namespace App\Http\Controllers;

use App\Models\Genre;
use Illuminate\Http\Request;

class GenreController extends Controller
{
    public function index()
	{
		$genre = Genre::all();

		return view('genre.index', compact('genre'));
	}

	public function tambah()
	{
		return view('genre.form');
	}

	public function simpan(Request $request)
	{
		$data = [
			'nama' => $request->nama,
		];

		Genre::create($data);

		return redirect()->route('genre');
	}

	public function edit($id_genre)
	{
		$genre = Genre::find($id_genre);

		return view('genre.form', ['genre' => $genre]);
	}

	public function update($id_genre, Request $request)
	{
		$data = [
			'nama' => $request->nama,
		];

		Genre::find($id_genre)->update($data);

		return redirect()->route('genre');
	}

	public function hapus($id_genre)
	{
		Genre::find($id_genre)->delete();

		return redirect()->route('genre');
	}

}
