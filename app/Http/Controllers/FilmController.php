<?php

namespace App\Http\Controllers;

use App\Models\Film;
use App\Models\Genre;
use Illuminate\Http\Request;

class FilmController extends Controller
{
    public function index()
	{
		$films = Film::all();

		return view('film.index', compact('films'));
	}

	public function tambah()
	{
		$genres = Genre::all();

		return view('film.form', compact('genres'));

	}

	public function simpan(Request $request)
	{
		$data = [
			'judul' => $request->judul,
			'ringkasan' => $request->ringkasan,
			'tahun' => $request->tahun,
			'poster' => $request->poster,
			'id_genre' => $request->id_genre,
		];

		Film::create($data);

		return redirect()->route('film');
	}

	public function edit($id_film)
	{
		$films = Film::find($id_film);
		$genres = Genre::all();

		return view('film.form', compact('genres', 'film'));
	}

	public function update($id_film, Request $request)
	{
		$data = [
			'judul' => $request->judul,
			'ringkasan' => $request->ringkasan,
			'tahun' => $request->tahun,
			'poster' => $request->poster,
			'id_genre' => $request->id_genre,
		];

		Film::find($id_film)->update($data);

		return redirect()->route('film');
	}

	public function hapus($id_film)
	{
		
		Film::find($id_film)->delete();

		return redirect()->route('film');
	}

}
