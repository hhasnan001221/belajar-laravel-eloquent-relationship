<?php

namespace App\Http\Controllers;

use App\Models\Cast;
use Illuminate\Http\Request;

class CastController extends Controller
{
	public function index()
	{
		$casts = Cast::all();

		return view('casts.index', compact('casts'));
	}

	public function tambah()
	{
		return view('casts.form');
	}

	public function simpan(Request $request)
	{
		$data = [
			'nama' => $request->nama,
			'umur' => $request->umur,
			'bio' => $request->bio,
		];

		Cast::create($data);

		return redirect()->route('casts');
	}

	public function edit($id_casts)
	{

		$casts = Cast::find($id_casts)->first();

		return view('casts.form', compact('casts'));
	}

	public function update($id_casts, Request $request)
	{
		$data = [
			'nama' => $request->nama,
			'umur' => $request->umur,
			'bio' => $request->bio,
		];

		Cast::find($id_casts)->update($data);

		return redirect()->route('casts');
	}

	public function hapus($id_casts)
	{
		Cast::find($id_casts)->delete();

		return redirect()->route('casts');
	}
}
