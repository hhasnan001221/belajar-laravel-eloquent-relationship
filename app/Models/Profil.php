<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    use HasFactory;

    protected $primaryKey = 'id_profil';

    protected $fillable = [
        'umur',
        'bio',
        'nama',
        'id_user',
    ];

    public function User()
    {
        return $this->belongsTo(User::class, 'id', 'id');
    }
}
