<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    use HasFactory;

    protected $table = 'films';

    protected $primaryKey = 'id_films';

	protected $fillable = ['judul', 'ringkasan', 'tahun', 'poster', 'id_genre'];

    public function Genre()
    {
        return $this->belongsTo(Genre::class, 'id_genre');
    }
}
